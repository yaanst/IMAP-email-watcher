import os
import yaml
import email
import ftplib
import logging
import imaplib
from time import sleep

#TODO: add error handling

def check_email(imap_con):
    #TODO modify search (use RECENT ? or is it in fetch command?)
    # and use 'FROM' 'TO'
    try:
        typ, data = imap_con.search(None, '(SUBJECT "Rotary")')
        if typ == 'OK':
            count = len(data[0].split())
            logging.info('Search complete. Found {} matching '\
                    'e-mails:\n{}'.format(count , data))
        else:
            logging.info('Search failed')
            raise
        return data[0].split()
    except imaplib.IMAP4_SSL.error as e:
        return None

def get_log_level(level):
    if type(level) == str:
        if level.lower() == 'debug':
            return logging.DEBUG
        elif level.lower() == 'info':
            return logging.INFO
        elif level.lower() == 'warning':
            return logging.WARNING
    return logging.info

def download_attachment(imap_con, email_id, tmp_dir):
    typ, data = imap_con.fetch(email_id, '(RFC822)')
    if typ == 'OK':
        logging.info('Fetched e-mail #{}'.format(email_id))
    else:
        logging.error('Fetching failed for email #{}'.format(email_id))
        raise
    
    text = data[0][1]
    msg = email.message_from_string(text.decode())
    logging.info('Downloading attachments...')

    for part in msg.walk():
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue

        filename = part.get_filename()
        att_path = os.path.join(tmp_dir, filename)
        
        if not os.path.isfile(att_path):
            try:
                f = open(att_path, 'wb')
                data = part.get_payload(decode=True)
                f.write(data)
                f.close()
                logging.info('\t{}'.format(filename))
            except:
                logging.error('\tError downloading or writing'\
                        ' {}'.format(att_path))
                raise
        else:
            logging.warning('Filepath {} already exists, download canceled to ' \
                    'avoid overwriting'.format(att_path))

    logging.info('Dowloading done')



def ftp_store(ftp_con, filepath, nas_filepath):
    f = open(filepath, 'rb')
    ftp_con.storbinary(nas_filepath, f)
    f.close()
    filename = filepath.split('/')[-1]
    logging.info('\t{}'.format(filename))

def main(config):
    e_host = config['EMAIL']['HOST']
    e_user = config['EMAIL']['USER']
    e_pwd = config['EMAIL']['PWD']
    e_dir = config['EMAIL']['DIR']

    n_host = config['NAS']['HOST']
    n_user = config['NAS']['USER']
    n_pwd = config['NAS']['PWD']
    n_dir = config['NAS']['DIR']

    log_path = config['LOG']['PATH']
    log_lvl = get_log_level(config['LOG']['LVL'])

    logging.basicConfig(filename=log_path, level=log_lvl,
            format='%(asctime)s:%(levelname)s:%(message)s')
            
    
    if not os.path.exists(e_dir):
        os.makedirs(e_dir)
    
    # Check for new e-mails
    while True:
        try:
            imap_con = imaplib.IMAP4_SSL(e_host, 993)
            typ, details = imap_con.login(e_user, e_pwd)
            if typ == 'OK':
                logging.info('IMAP connection into {} as {} established'.format(e_host, e_user))
            else:
                logging.error('IMAP connection '\
                        'failed:\n\t{}\n\t{}'.format(e_host, e_user))
                raise

            typ, data = imap_con.select(mailbox='INBOX', readonly=True)
            if typ == 'OK':
                logging.info('INBOX folder selected as readonly')
            else:
                logging.error('INBOX folder selection failed')
                raise
        except Exception as e:
            logging.error('Error "{}" in setting up the connection prevented '\
                    'the script from executing succesfully'.format(e))
            return
            

        try:
            email_ids = check_email(imap_con)
            if email_ids:
                for email_id in email_ids:
                        download_attachment(imap_con, email_id, e_dir)

                ftp_con = ftplib.FTP(n_host, n_user, n_pwd)
                ftp_con.connect()
                logging.info('FTP connection to {} as {} '\
                        'established'.format(n_host, n_user))

                e_dir_b = os.fsencode(e_dir)

                # Upload files to NAS
                logging.info('Uploading files...')
                for filename_b in os.listdir(e_dir_b):
                    filename = os.fsdecode(filename_b)

                    if filename.endswith('.csv'):
                        ftp_store(ftp_con, filename, n_dir)
                        os.remove(os.join(e_dir, filename))
                        logging.info('\t{}'.format(filename))
                logging.info('Uploading done')

            ftp_con.quit()
            logging.info('FTP connection closed')

        except ftplib.all_errors as e:
            logging.warning('FTP connection or upload failed:\n'\
                    '\thost:{}\n\tuser:{}'.format(n_host, n_user))

        except Exception as e:
            logging.error('Error "{}" in retrieving or saving attachment '\
                    'prevented the script from executing successfully'.format(e))
            return


        # Stop imap connection
        imap_con.close()
        imap_con.logout()
        logging.info('IMAP connection closed')


if __name__ == '__main__':
    config = yaml.load(open('config.yml'))
    main(config)
