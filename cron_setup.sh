#!/usr/bin/bash
# Run every day at 02:00
(crontab -l 2>/dev/null; echo "0 2 * * * cd /opt/email_watcher/ && /opt/venv/bin/python /opt/email_watcher/main.py") | crontab -
